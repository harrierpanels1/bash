#!/bin/bash

# Check if jq is installed
if ! command -v jq &>/dev/null; then
    echo "jq command not found. Installing jq..."
    sudo yum install -y jq
fi

# Check if the output.txt file is provided as an argument
if [[ $# -eq 0 ]]; then
    echo "Please provide the path to the output.txt file as an argument."
    exit 1
fi

output_file="output.json"

# Extract test name
test_name=$(grep -oP '(?<=\[ ).+?(?= \])' "$1")

# Extract test results and create JSON array
results=$(grep -oP '^[not ok|ok].*' "$1")
test_array=()
while IFS= read -r result; do
    status=$(echo "$result" | grep -oP '^not')
    name=$(echo "$result" | grep -oP '[^"]+(?=, \d+ms$)' | awk -F '  +' '{print $3}')
    duration=$(echo "$result" | grep -oP '\d+ms$')
    if [[ "$status" == "not" ]]; then
        test_array+=("{\"name\":\"$name\",\"status\":false,\"duration\":\"$duration\"}")
    else
        test_array+=("{\"name\":\"$name\",\"status\":true,\"duration\":\"$duration\"}")
    fi
done <<< "$results"

# Extract summary
summary=$(grep -oP '\d+ \(of \d+\) tests passed, \d+ tests failed, rated as \d+(\.\d+)?%' "$1")
success=$(echo "$summary" | grep -oP '\d+ \(of' | grep -oP '\d+')
failed=$(echo "$summary" | grep -oP 'passed, \d+ tests' | grep -oP '\d+')
rating=$(echo "$summary" | grep -oP 'rated as \d+(\.\d+)?%' | grep -oP '\d+(\.\d+)?')
duration=$(grep -oP 'spent \d+ms' "$1" | grep -oP '\d+')
duration_with_unit="${duration}ms"

# Create JSON object with formatted indentation using jq
json_object=$(jq -n \
    --arg test_name "$test_name" \
    --argjson test_array "$(IFS=','; echo "[${test_array[*]}]")" \
    --arg success "$success" \
    --arg failed "$failed" \
    --arg rating "$rating" \
    --arg duration "$duration_with_unit" \
    '{
      "testName": $test_name,
      "tests": $test_array,
      "summary": {
        "success": ($success | tonumber),
        "failed": ($failed | tonumber),
        "rating": ($rating | tonumber),
        "duration": $duration
      }
    }' | jq --indent 4 '.')

# Save formatted JSON object to output file
echo "$json_object" >"$output_file"

echo "Conversion complete. Output saved to $output_file."
